FROM hassanamin994/node_ffmpeg:6

WORKDIR /tvw_exporter

COPY package*.json ./
RUN npm install

COPY . .
RUN echo "GOOGLE_APPLICATION_CREDENTIALS=/tvw_exporter/gsc_creds.json" >> .env

# ADD AWS CREDENTIALS FILE
ARG AWS_KEYS_FILE_BASE64
RUN mkdir ~/.aws
RUN echo ${AWS_KEYS_FILE_BASE64} | base64 --decode > ~/.aws/credentials

CMD ["npm", "run", "docker:prod"]