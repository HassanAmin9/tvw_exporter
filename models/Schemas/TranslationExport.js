const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { SchemaNames } = require('./utils/schemaNames');

const EXPORT_REQUEST_STATUS = ['pending', 'approved', 'declined'];
const STATUS_ENUM = ['queued', 'processing', 'done', 'failed'];

const TranslationExport = new Schema({
    organization: { type: Schema.Types.ObjectId, ref: SchemaNames.organization },
    article: { type: Schema.Types.ObjectId, ref: SchemaNames.article },
    video: { type: Schema.Types.ObjectId, ref: SchemaNames.video },

    progress: { type: Number, default: 0 },
    exportRequestStatus: { type: String, enum: EXPORT_REQUEST_STATUS, default: 'pending' },
    status: { type: String, enum: STATUS_ENUM, default: 'queued' },

    videoUrl: { type: String },
    slidesArchiveUrl: { type: String },
    audiosArchiveUrl: { type: String },
    subtitledVideoUrl: { type: String },
    subtitleUrl: { type: String },

    audiosArchiveProgress: { type: Number },
    subtitledVideoProgress: { type: Number },
    subtitleProgress: { type: Number },


    voiceVolume: { type: Number, default: 1 },
    backgroundMusicVolume: { type: Number, default: 1 },
    normalizeAudio: { type: Boolean, default: true },

    dir: { type: String },
})

module.exports = { TranslationExport };