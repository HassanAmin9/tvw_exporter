const path = require('path');
const uuid = require('uuid').v4;
const fs = require('fs');

const storageService = require('../vendors/storage');
const videoHandler = require('../dbHandlers/video');
const articleHandler = require('../dbHandlers/article');
const translationExportHandler = require('../dbHandlers/translationExport');

const async = require('async');
const queues = require('../constants').queues;
const utils = require('../utils');
const converter = require('../converter');

const DEFAULT_AUDIO_FADE = { fadeDuration: 20, durationType: 'percentage' };

const onExportArticleTranslation = channel => msg => {
    const { translationExportId } = JSON.parse(msg.content.toString());
    console.log('got request to export', translationExportId)
    const tmpFiles = [];
    let article;
    let allSubslides = [];
    let finalSubslides = [];
    const tmpDirName = uuid();
    const tmpDirPath = path.join(__dirname, `../${tmpDirName}`);
    let originalVideoPath = '';
    fs.mkdirSync(tmpDirPath);
    let video;
    let translationExport;

    translationExportHandler.findById(translationExportId)
        .populate('article')
        .populate('video')
        .then((a) => {
            return new Promise((resolve, reject) => {
                if (!a) return reject(new Error('Invalid translation export id'));
                // console.log('found article', a)
                translationExport = a.toObject();
                article = translationExport.article;
                video = translationExport.video;
                originalVideoPath = path.join(tmpDirPath, `original-video-${uuid()}-${video.url.split('.').pop()}`)
                article.slides.sort((a, b) => a.positon - b.position).forEach(slide => {
                    slide.content.sort((a, b) => a.position - b.position).forEach((subslide) => {
                        allSubslides.push(subslide);
                    })
                });
                // Update status to processing
                translationExportHandler.updateById(translationExportId, { status: 'processing' }).then(() => {
                })
                .catch(err => { console.log(err) });

                allSubslides = allSubslides.sort((a, b) => a.startTime - b.startTime).map((s,index) => ({ ...s, position: index }));
                allSubslides
                const downloadMediaFuncArray = [];

                console.log('downloading media')
                downloadMediaFuncArray.push((cb) => {
                    console.log('Downloading original video');
                    const videoUrl = translationExport.article.videoSpeed && translationExport.article.videoSpeed !== 1 && translationExport.article.videoUrl ? translationExport.article.videoUrl : video.url;
                    utils.downloadFile(videoUrl, originalVideoPath)
                    .then(() => {
                        return cb();
                    })
                    .catch(cb);
                })

                allSubslides.forEach((subslide) => {
                    downloadMediaFuncArray.push((cb) => {
                        const audioPath = path.join(__dirname, `../${tmpDirName}`, `single-audio-${uuid()}.${subslide.audio.split('.').pop()}`);
                        utils.downloadFile(subslide.audio, audioPath)
                        .then((audioPath) => {
                            subslide.audioPath = audioPath;
                            return cb()
                        })
                        .catch(cb)
                    })
                })

                async.parallelLimit(downloadMediaFuncArray, 2, (err) => {
                    if (err) return reject(err);
                    return resolve(allSubslides);
                })
            })
        })
        .then(allSubslides => {
            return new Promise((resolve, reject) => {
                const addAudioFuncArray = [];
                allSubslides.forEach((subslide) => {
                    if (subslide.audioPath) {
                        addAudioFuncArray.push((cb) => {
                            // Convert any extension to mp3
                            const audioExt = subslide.audioPath.split('.').pop().toLowerCase();
                            if ( audioExt !== 'mp3') {
                                console.log('converting to mp3')
                                converter.convertToMp3(subslide.audioPath)
                                .then((newPath) => {
                                    subslide.audioPath = newPath;
                                    return cb();
                                })
                                .catch((err) => {
                                    return cb(err);  
                                })
                            } else {
                                return cb()
                            }
                        })
                    }
                })
                async.parallelLimit(addAudioFuncArray, 1, (err) => {
                    if (err) return reject(err);
                    return resolve(allSubslides);
                })
            })
        })
        // increase volume of recorded audios
        .then((allSubslides) => {
            return new Promise((resolve, reject) => {
                // Increase volume
                if (!translationExport.voiceVolume || translationExport.voiceVolume === 1) return resolve(allSubslides);
                const increaseVolumeFuncArray = [];
                allSubslides.forEach((subslide) => {
                    if (subslide.speakerProfile && subslide.speakerProfile.speakerNumber !== -1) {
                        increaseVolumeFuncArray.push(cb => {
                            const targetPath = path.join(__dirname, `../${tmpDirName}`, `increaseVolume-audio-${uuid()}.${subslide.audioPath.split('.').pop()}`);
                            converter.changeAudioVolume(subslide.audioPath, targetPath, translationExport.voiceVolume)
                            .then((outPath) => {
                                console.log('increase volume to ', translationExport.voiceVolume)
                                subslide.audioPath = outPath;
                                cb();
                            })
                            .catch(err => {
                                console.log('error changing audio volume', err);
                                cb();
                            })
                        })

                    }
                })

                async.series(increaseVolumeFuncArray, (err) => {
                    if (err) {
                        console.log(err);
                    }
                    return resolve(allSubslides)
                })
            })
        })
        // Fade in/out effects on audios
        .then((allSubslides) => {
            return new Promise((resolve, reject) => {
                allSubslides = allSubslides.sort((a, b) => a.startTime - b.startTime);
                const firstTranslationSlideIndex = allSubslides.findIndex((s) => s.speakerProfile.speakerNumber !== -1);
                const lastTranslationSlideIndex = (allSubslides.length - 1) - allSubslides.slice().reverse().findIndex((s) => s.speakerProfile.speakerNumber !== -1);
                let backgroundMusicSlidesIndexes  = [];
                allSubslides.forEach((s, index) => {
                    if (s.speakerProfile && s.speakerProfile.speakerNumber === -1) {
                        backgroundMusicSlidesIndexes.push(index);
                    }
                })
                const fadevideoFuncArray = [];

                /* Temporarly disable fading in first translation slide and 
                    fading out last translations slide
                */
                /*
                // First translation slide
                fadevideoFuncArray.push((cb) => {
                    const firstSubslide = allSubslides[firstTranslationSlideIndex];
                    const newaudioPath = path.join(__dirname, `../${tmpDirName}`, `faded-video-${uuid()}.${firstSubslide.audioPath.split('.').pop()}`);
                    converter.fadeAudio(allSubslides[firstTranslationSlideIndex].audioPath, 'in', DEFAULT_AUDIO_FADE, newaudioPath)
                    .then(() => {
                        allSubslides[firstTranslationSlideIndex].audioPath = newaudioPath;
                        return cb();
                    })
                    .catch((err) => {
                        console.log(err);
                        return cb();
                    })
                })
                // Fade out last translation slide
                fadevideoFuncArray.push((cb) => {
                    const lastSubslide = allSubslides[lastTranslationSlideIndex];
                    const newaudioPath = path.join(__dirname, `../${tmpDirName}`, `faded-audio-${uuid()}.${lastSubslide.audioPath.split('.').pop()}`);
                    converter.fadeAudio(allSubslides[lastTranslationSlideIndex].audioPath, 'out', DEFAULT_AUDIO_FADE, newaudioPath)
                    .then(() => {
                        allSubslides[lastTranslationSlideIndex].audioPath = newaudioPath;
                        return cb();
                    })
                    .catch((err) => {
                        console.log(err);
                        return cb();
                    })
                })
                */
                // Fade in all background music slides
                if (!video.backgroundMusicUrl) {
                    backgroundMusicSlidesIndexes.forEach((subslideIndex) => {
                        fadevideoFuncArray.push((cb) => {
                            const subslide = allSubslides[subslideIndex];
                            const newaudioPath = path.join(__dirname, `../${tmpDirName}`, `faded-audio-${uuid()}.${subslide.audioPath.split('.').pop()}`);
                            converter.fadeAudio(allSubslides[subslideIndex].audioPath, 'both', DEFAULT_AUDIO_FADE, newaudioPath)
                            .then((audioPath) => {
                                allSubslides[subslideIndex].audioPath = audioPath;
                                // Fade in the following subslide if it exists
                                if (subslideIndex + 1 !== firstTranslationSlideIndex &&
                                    subslideIndex + 1 !== lastTranslationSlideIndex && 
                                    allSubslides[subslideIndex + 1] &&
                                    allSubslides[subslideIndex + 1].speakerProfile &&
                                    allSubslides[subslideIndex + 1].speakerProfile.speakerNumber !== -1
                                    ) {
                                        const nextSubslide = allSubslides[subslideIndex + 1];
                                        const newNextaudioPath = path.join(__dirname, `../${tmpDirName}`, `faded-audio-${uuid()}.${nextSubslide.audioPath.split('.').pop()}`);
                                        converter.fadeAudio(nextSubslide.audioPath, 'in', DEFAULT_AUDIO_FADE, newNextaudioPath)
                                        .then((audioPath) => {
                                            allSubslides[subslideIndex + 1].audioPath = audioPath;
                                            return cb();
                                        })
                                        .catch(err => {
                                            console.log(err);
                                            return cb()
                                        })
                                    } else {
                                        return cb();
                                    }
                            })
                            .catch((err) => {
                                console.log(err);
                                return cb();
                            })
                        })
                    })
                }

                async.series(fadevideoFuncArray, (err) => {
                    if (err) {
                        console.log('error applying fade ', err);
                    }

                    return resolve(allSubslides)
                })
            })
        })
        // Extend audios to videos durations and combine into one file
        .then(allSubslides => {
            return new Promise((resolve, reject) => {
                const extendAudiosFuncArray = [];
                allSubslides.forEach((subslide) => {
                    extendAudiosFuncArray.push(cb => {
                        const targetPath = path.join(tmpDirPath, `extended-audio-${uuid()}.${subslide.audioPath.split('.').pop()}`);
                        converter.extendAudioDuration(subslide.audioPath, targetPath, subslide.endTime - subslide.startTime)
                        .then((newPath) => {
                            subslide.audioPath = newPath;
                            cb();
                        })
                        .catch(cb);
                    })
                })

                async.parallelLimit(extendAudiosFuncArray, 2, (err) => {
                    if (err) return reject(err);
                    return resolve(allSubslides);
                })
            })
        })
        // Convert background music slides to silent audios if the video already have backgroundMusicUrl
        .then((allSubslides) => {
            return new Promise((resolve) => {
                if (!video.backgroundMusicUrl) return resolve(allSubslides);
                const generateSilentAudioFuncArray = [];
                allSubslides.filter((s) => s.speakerProfile && s.speakerProfile.speakerNumber === -1).forEach((subslide) => {
                    generateSilentAudioFuncArray.push((cb) => {
                        const newaudioPath = path.join(__dirname, `../${tmpDirName}`, `silent-audio-${uuid()}.${subslide.audioPath.split('.').pop()}`);
                   
                        converter.generateSilentFile(newaudioPath, subslide.endTime - subslide.startTime)
                        .then(() => {
                            subslide.audioPath = newaudioPath;
                            return cb();
                        })
                        .catch(err => {
                            console.log('error generating silent file', err);
                            return cb();
                        })
                    })
                })
                async.series(generateSilentAudioFuncArray, () => {
                    resolve(allSubslides);
                })
            })
        })
        // Concat audios
        .then((allSubslides) => {
            return new Promise((resolve, reject) => {
                const finalAudioPath = path.join(tmpDirPath, `final-audio-${uuid()}.${allSubslides[0].audioPath.split('.').pop()}`);
                converter.combineAudios(allSubslides.map((s) => s.audioPath), finalAudioPath)
                .then(() => {
                    return resolve(finalAudioPath);
                })
                .catch(reject);
            })
        })
        // Normalize audio step
        .then((finalAudioPath) => {
            return new Promise((resolve) => {
                if (!translationExport.normalizeAudio) return resolve(finalAudioPath);
                const normalizedFinalAudioPath = path.join(tmpDirPath, `normalized-final-audio-${uuid()}.${finalAudioPath.split('.').pop()}`);
                converter.normalizeAudio(finalAudioPath, normalizedFinalAudioPath)
                .then(() => {
                    return resolve(normalizedFinalAudioPath);
                })
                .catch(err => {
                    console.log('error normalizing audio', err);
                    return resolve(finalAudioPath);
                })
            })
        })
        // Overlay audio on video
        .then((finalAudioPath) => {
            const finalVideoPath = path.join(tmpDirPath, `final-video-${uuid()}.${video.url.split('.').pop()}`)
            return converter.addAudioToVideo(originalVideoPath, finalAudioPath, finalVideoPath)
        })
        .then((finalVideoPath) => {
            // Overlay background music if it exists
            return new Promise((resolve, reject) => {
                if (!video.backgroundMusicUrl) return resolve(finalVideoPath);
                const overlayedFinalVideoPath = path.join(__dirname, `../${tmpDirName}`, `overlayed-video-${uuid()}.${finalVideoPath.split('.').pop()}`);
                const backgroundMusicPath = path.join(__dirname, `../${tmpDirName}`, `background-music-${uuid()}.${video.backgroundMusicUrl.split('.').pop()}`);
                utils.downloadFile(video.backgroundMusicUrl, backgroundMusicPath)
                .then(() => {
                    return converter.overlayAudioOnVideo(finalVideoPath, backgroundMusicPath, translationExport.backgroundMusicVolume || 1, overlayedFinalVideoPath)
                })
                .then(() => {
                    return resolve(overlayedFinalVideoPath);
                })
                .catch((err) => {
                    console.log('error overlaying background music', err);
                    return resolve(finalVideoPath);
                })
            })
        })
        .then((finalVideoPath) => {
            console.log('final path', finalVideoPath);
            return storageService.saveFile('translationExports', `${translationExport.dir}/${article.langCode || article.langName}_${article.title}.${finalVideoPath.split('.').pop()}`, fs.createReadStream(finalVideoPath)); 
        })
        .then(uploadRes => {
            return new Promise((resolve, reject) => {
                translationExportHandler.updateById(translationExportId, { status: 'done', progress: 100, videoUrl: uploadRes.url }).then(() => {
                    channel.sendToQueue(queues.EXPORT_ARTICLE_TRANSLATION_FINISH, new Buffer(JSON.stringify({ translationExportId })), { persistent: true });
                    channel.ack(msg);
                    console.log('done')
                    return resolve()
                })
                .catch(reject);
            })
        })
        .then(() => {
            utils.cleanupDir(path.join(__dirname, `../${tmpDirName}`))
        })
        .catch(err => {
            utils.cleanupDir(path.join(__dirname, `../${tmpDirName}`))
            console.log(err,' error from catch');
            channel.ack(msg);
            translationExportHandler.updateById(translationExportId, { status: 'failed' }).then(() => {});
            channel.sendToQueue(queues.EXPORT_ARTICLE_TRANSLATION_FINISH, new Buffer(JSON.stringify({ translationExportId })), { persistent: true });
        })
}

function updateTranslationExportProgress(translationExportId, progress) {
    translationExportHandler.updateById(translationExportId, { progress })
    .then(() => {
        console.log('progress', progress)
    })
    .catch(err => {
        console.log('error updating progres', err);
    })
}

module.exports = onExportArticleTranslation;